'use strict'

const readSiteManifest = require('./read-site-manifest')

async function importSiteManifest (playbook, contentCatalog, primarySiteManifestUrl, primarySiteUrl,
  partialComponents) {
  if (!primarySiteManifestUrl) primarySiteManifestUrl = primarySiteUrl + '/site-manifest.json'
  const x = await readSiteManifest(playbook, primarySiteManifestUrl)
  const { components, url: siteUrlFromManifest } = x
  if (!components) return
  if (!primarySiteUrl) primarySiteUrl = siteUrlFromManifest
  const contents = Buffer.alloc(0)
  components.forEach(({ name, title, versions }) => {
    const component = contentCatalog.getComponent(name)
    const latestInCurrentSite = component ? component.latest : undefined
    versions.forEach(({ displayVersion, pages, version, url: startUrl }) => {
      // NOTE skip component version and all its pages if already in this site
      let componentVersion
      if (component && (componentVersion = contentCatalog.getComponentVersion(component, version))) {
        if (!partialComponents) return
      } else {
        contentCatalog.registerComponentVersion(name, version, { displayVersion, title })
        componentVersion = contentCatalog.getComponentVersion(component || name, version)
        // NOTE origin.site indicates this component version is not in this site, but rather an external reference
        // Q should we rename origin to external?
        componentVersion.origin = { site: primarySiteUrl }
        componentVersion.url = primarySiteUrl + startUrl
      }
      pages.forEach(({ module: module_ = 'ROOT', path, title: doctitle, url }) => {
        const src = { component: name, version, module: module_, relative: path, family: 'page', basename: '' }
        if (!partialComponents || !contentCatalog.getById(src)) {
          // NOTE we must include (empty) contents just in case this page gets referenced by an include directive
          contentCatalog.addFile({
            contents,
            src,
            pub: { url: primarySiteUrl + url },
            origin: { site: primarySiteUrl },
          })
          // FIXME we need a way to addFile without setting out (maybe check if out is set, but undefined?)
          delete src.basename
          const file = contentCatalog.getById(src)
          //file.asciidoc = { doctitle }
          delete file.out
        }
      })
    })
    if (component) {
      component.preferred = component.latest
      component.latest = latestInCurrentSite
    } else {
      contentCatalog.getComponent(name).origin = { site: primarySiteUrl }
    }
  })
}

module.exports = importSiteManifest
