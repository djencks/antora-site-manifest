'use strict'

const createSiteFile = require('./create-site-file')

function exportSiteManifest (components, allPages, siteUrl) {
  components = [...components].sort(({ name: nameA }, { name: nameB }) => nameA.localeCompare(nameB))
  const pagesByComponentVersion = allPages.reduce((accum, { asciidoc, pub, src }) => {
    const key = `${src.version}@${src.component}`
    ;(accum[key] || (accum[key] = [])).push({
      module: src.module,
      path: src.relative,
      url: pub.url,
      title: asciidoc.doctitle,
    })
    return accum
  }, {})
  const manifestData = {
    version: 2,
    generated: +new Date(),
    url: siteUrl,
    components: components
      .map(({ name, title, versions, latest: { version: latest } }) => ({
        name,
        title,
        latest,
        versions: versions
          .filter(({ origin = {} }) => !origin.site)
          .reduce((accum, { version, displayVersion, url }) => {
            const pages = pagesByComponentVersion[`${version}@${name}`]
            // NOTE don't add version if there are no pages
            if (!pages) return
            pages.sort(
              ({ module: moduleA, path: pathA }, { module: moduleB, path: pathB }) =>
                moduleA.localeCompare(moduleB) || pathA.localeCompare(pathB)
            )
            if (displayVersion === version) displayVersion = undefined
            return accum.concat({ version, displayVersion, url, pages })
          }, []),
      }))
      .filter((component) => component.versions),
  }
  return createSiteFile('site-manifest.json', JSON.stringify(manifestData, null, 2), 'application/json')
}

module.exports = exportSiteManifest
