'use strict'

const exportSiteManifest = require('./export-site-manifest')
const exportSiteNavigationData = require('./export-site-navigation-data')
const importSiteManifest = require('./import-site-manifest')

/**
 * Antora Site Manifest Plugin
 *
 * Configured with:
 *  * 0 or more input site manifest locations
 *  ** each can specify the site url or site manifest url
 *  * a flag for an output manifest
 *  * a further flag for output nav manifest
 *  * something for sorting the components.
 * Adds contents of input manifests to catalog as non-publishable files, and if configured adds an
 * output site manifest to the site.
 */
module.exports.register = (eventEmitter, config = {}) => {
  const importManifests = config.importManifests
  const exportSiteManifestFlag = config.exportSiteManifest
  const exportSiteNavigationDataFlag = config.exportSiteNavigationData
  const siteComponentOrder = config.siteComponentOrder
  const partialComponents = !!config.partialComponents
  var theContentCatalog
  const newSiteFiles = []

  if (importManifests) {
    eventEmitter.on('afterClassifyContent', (context, contentCatalog) => {
      const playbook = context.playbook ? context.playbook : context
      return Promise.all(importManifests.map(({ primarySiteManifestUrl, primarySiteUrl }) =>
        importSiteManifest(playbook, contentCatalog, primarySiteManifestUrl, primarySiteUrl, partialComponents)))
    })
  }

  if (exportSiteManifestFlag) {
    eventEmitter.on('beforeConvertDocuments', ({ contentCatalog }) => {
      theContentCatalog = contentCatalog
    })

    eventEmitter.on('afterConvertDocuments', (context, pages) => {
      const playbook = context.playbook ? context.playbook : context
      if (playbook.site && playbook.site.url) {
        const components = theContentCatalog.getComponents()
        newSiteFiles.push(exportSiteManifest(components, pages, playbook.site.url))
        if (exportSiteNavigationDataFlag) {
          newSiteFiles.push(exportSiteNavigationData(components, siteComponentOrder))
        }
      }
    })

    eventEmitter.on('afterMapSite', (playbook, siteFiles) => siteFiles.push(...newSiteFiles))
  }
}
