'use strict'

const expandPath = require('@antora/expand-path-helper')
const fs = require('fs-extra')
const get = require('got')
const getCacheDir = require('cache-directory')
const ospath = require('path')
const { ungzip: gunzip } = require('node-gzip')

const URI_SCHEME_RX = /^https?:\/\//

function downloadSiteManifest (playbook, url) {
  const { cacheDir, fetch } = playbook.runtime || {}
  const resolvedCacheDir = ensureCacheDir(cacheDir, playbook.dir || '.')
  const cacheFile = ospath.join(resolvedCacheDir, 'primary-site-manifest.json')
  return (fetch ? Promise.resolve(false) : fs.pathExists(cacheFile)).then((exists) =>
    exists
      ? cacheFile
      : fs
        .ensureDir(resolvedCacheDir)
        .then(() =>
          get(url, { resolveBodyOnly: true, responseType: 'buffer' }).then((body) =>
            fs.outputFile(cacheFile, body).then(() => cacheFile)
          )
        )
  )
}

function ensureCacheDir (cacheDir, startDir) {
  return cacheDir == null
    ? getCacheDir('antora') || ospath.resolve('.antora/cache')
    : expandPath(cacheDir, '~+', startDir)
}

function isUrl (string) {
  return ~string.indexOf('://') && URI_SCHEME_RX.test(string)
}

async function readSiteManifest (playbook, manifestUrl) {
  const manifestPath = await (isUrl(manifestUrl)
    ? downloadSiteManifest(playbook, manifestUrl)
    : expandPath(manifestUrl, '~+', playbook.dir))
  try {
    return JSON.parse(
      await (manifestPath.endsWith('.gz') ? fs.readFile(manifestPath).then(gunzip) : fs.readFile(manifestPath, 'utf-8'))
    )
  } catch (e) {
    return {}
  }
}

module.exports = readSiteManifest
