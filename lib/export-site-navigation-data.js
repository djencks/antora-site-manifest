'use strict'

const createSiteFile = require('./create-site-file')
const customSortComponents = require('./custom-sort-components')
const uglify = require('uglify-js')

function exportSiteNavigationData (components, componentOrder = undefined) {
  const navigationData = customSortComponents(components, componentOrder).map(({ name, title, versions }) => ({
    name,
    title,
    versions: versions.map(({ version, displayVersion, navigation = [] }) => {
      if (displayVersion === version) displayVersion = undefined
      return { version, displayVersion, sets: strain(navigation) }
    }),
  }))
  const navigationDataSource = uglify.minify(`siteNavigationData = ${JSON.stringify(navigationData)}`, {
    compress: false,
    mangle: false,
    output: { max_line_len: 500 },
  }).code
  return createSiteFile('site-navigation-data.js', navigationDataSource, 'application/javascript')
}

function strain (items) {
  return items.map((item) => {
    const strainedItem = { content: item.content }
    var urlType = item.urlType
    if (urlType) {
      strainedItem.url = item.url
      if (urlType !== 'internal') strainedItem.urlType = item.urlType
    }
    if (item.items) strainedItem.items = strain(item.items)
    return strainedItem
  })
}

module.exports = exportSiteNavigationData
